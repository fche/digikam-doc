# German translations for Digikam Manual package.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Digikam Manual package.
# Automatically generated, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: Digikam Manual 8.0.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-05 00:40+0000\n"
"PO-Revision-Date: 2022-12-29 16:23+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: ../../maintenance_tools/maintenance_duplicates.rst:1
msgid "digiKam Maintenance Tool Find Duplicates"
msgstr ""

#: ../../maintenance_tools/maintenance_duplicates.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy"
msgstr ""

#: ../../maintenance_tools/maintenance_duplicates.rst:14
msgid "Find Duplicates"
msgstr ""

#: ../../maintenance_tools/maintenance_duplicates.rst:16
msgid "Contents"
msgstr ""

#: ../../maintenance_tools/maintenance_duplicates.rst:20
msgid ""
"The Find Duplicates Tool is doing the same as the Find duplicates button in "
"the :ref:`the Similarity View <similarity_view>`, but here you can combine "
"it with other maintenance operations and you have the chance to check “Work "
"on all processor cores” under Common Options (see above) to speed up the "
"process."
msgstr ""

#: ../../maintenance_tools/maintenance_duplicates.rst:24
msgid "This process provides two options to find duplicates items:"
msgstr ""

#: ../../maintenance_tools/maintenance_duplicates.rst:26
msgid ""
"**Similarity Range**: the lower and higher values to define the range of "
"similarity in percents."
msgstr ""

#: ../../maintenance_tools/maintenance_duplicates.rst:28
msgid ""
"**Restriction**: this option restrict the duplicate search with some "
"criteria, as to limit search to the album of reference image, or to exclude "
"the album of reference image of the search."
msgstr ""

#: ../../maintenance_tools/maintenance_duplicates.rst:30
msgid ""
"While the find duplicates process is under progress, notification on the "
"bottom right of main windows will be visible to indicate the amount of items "
"already done."
msgstr ""

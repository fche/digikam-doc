# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Digikam Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Digikam Manual 8.0.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-27 00:51+0000\n"
"PO-Revision-Date: 2023-01-11 09:50+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: digiKam\n"

#: ../../image_editor.rst:1
msgid "Using digiKam to Edit Your Photographs"
msgstr "Usar o digiKam para Editar as suas Fotografias"

#: ../../image_editor.rst:1
#, fuzzy
#| msgid ""
#| "digiKam, documentation, user manual, photo management, open source, free, "
#| "help, learn, image editor"
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"help, learn, image, editor"
msgstr ""
"digiKam, documentação, manual do utilizador, gestão de fotografias, código "
"aberto, livre, ajudar, aprender, editor de imagens"

#: ../../image_editor.rst:15
msgid "Image Editor"
msgstr "Editor de Imagem"

#: ../../image_editor.rst:19
msgid "This section explain how to use the digiKam image editor."
msgstr "Esta secção explica como usar o editor de imagens do digiKam."

#: ../../image_editor.rst:21
msgid "Contents:"
msgstr "Conteúdo:"

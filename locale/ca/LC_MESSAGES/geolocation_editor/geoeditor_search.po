# Translation of docs_digikam_org_geolocation_editor___geoeditor_search.po to Catalan
# Copyright (C) 2023 This_file_is_part_of_KDE
# Licensed under the <a href="https://spdx.org/licenses/GFDL-1.2-or-later.html">terms of the GNU Free Documentation License 1.2</a> unless stated otherwise
# This file is distributed under the same license as the digiKam Manual package.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: digikam-doc\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-05 00:40+0000\n"
"PO-Revision-Date: 2023-01-06 18:55+0100\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Poedit 3.1.1\n"

#: ../../geolocation_editor/geoeditor_search.rst:1
msgid "digiKam Geolocation Editor Search Tool"
msgstr "Eina de cerca de l'editor de geolocalització del digiKam"

#: ../../geolocation_editor/geoeditor_search.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, location, search, geoname"
msgstr ""
"digiKam, documentació, manual d'usuari, gestió de les fotografies, codi "
"obert, lliure, aprenentatge, fàcil, ubicació, cerca, geonom"

#: ../../geolocation_editor/geoeditor_search.rst:14
msgid "Search Tool"
msgstr "Eina de cerca"

#: ../../geolocation_editor/geoeditor_search.rst:16
msgid "Contents"
msgstr "Contingut"

#: ../../geolocation_editor/geoeditor_search.rst:18
msgid ""
"In the **Search** tab you can look up places by their geographic names using "
"public geoname services **GeoNames** and **OpenStreetMap**. You just type in "
"a name of a place (city, monument, hotel, ...) into the input field at the "
"top and click on **Search** button. In the result list below you will get "
"something like this:"
msgstr ""
"A la pestanya **Cerca** podreu cercar llocs pels seus noms geogràfics "
"utilitzant els serveis públics de noms geogràfics **GeoNames** i "
"**OpenStreetMap**. Només cal escriure el nom d'un lloc (ciutat, monument, "
"hotel...) a dins del camp d'entrada a la part superior i feu clic en el botó "
"**Cerca**. A la llista de resultats que hi ha a sota obtindreu alguna cosa "
"com això:"

#: ../../geolocation_editor/geoeditor_search.rst:24
msgid ""
"The digiKam Geolocation Editor Searching For a Place Named *Paris* Over The "
"World"
msgstr ""
"L'editor de geolocalització del digiKam cerca un lloc anomenat *París* al món"

#: ../../geolocation_editor/geoeditor_search.rst:26
msgid ""
"Obviously *Paris* does not only exist in France. So if you were looking for "
"the capital of France you can either scroll down the list until you find an "
"entry that undoubtedly belongs to there like *Arc de Triomphe* and click on "
"it or specify your search a little bit more precise like *Paris, Arc de "
"Triomphe* or *Paris, France*. Once you click on an entry in the list the map "
"will become centered to that place and you can now zoom in with the zoom-in "
"button (second from the left below the map). Using this example with "
"GeoNames you will notice that you still get a lot of results and even two "
"labeled only *Arc de Triomphe*, the second one a street a few hundred meters "
"away from the arch. With OpenStreetMap you get only one result. You got to "
"play a bit with the different services in different regions, with more or "
"less precise search terms and then you will get an idea about how to best "
"conduct your searches."
msgstr ""
"Òbviament, *París* no només existeix a França. Així que si esteu cercant la "
"capital de França, podreu desplaçar-vos per la llista fins a trobar una "
"entrada que sens dubte hi pertanyi com *Arc de Triomf*, i feu clic o "
"especifiqueu una cerca una mica més precisa com *París, Arc de Triomf* o "
"*París, França*. Una vegada feu clic en una entrada a la llista, el mapa se "
"centrarà en aquest lloc i ara podreu apropar amb el botó de zoom (segon des "
"de l'esquerra, a sota del mapa). Utilitzant aquest exemple amb GeoNames, us "
"adonareu que així i tot, encara s'obtenen una gran quantitat de resultats, "
"fins i tot, dos etiquetats únicament amb *Arc de triomf*, el segon, un "
"carrer a uns quants centenars de metres de distància de l'arc. Amb "
"OpenStreetMap s'obté un sol resultat. Haureu de jugar una mica amb els "
"diferents serveis en diferents regions, amb termes de cerca més o menys "
"precisos i després obtindreu una idea sobre com millorar les vostres cerques."

#: ../../geolocation_editor/geoeditor_search.rst:28
msgid ""
"For the buttons right above the result list refer to their tooltips! The "
"context menu on search result entries offers:"
msgstr ""
"Per als botons de la dreta, just a sobre de la llista de resultats es "
"refereixen als seus consells d'eina! El menú contextual de les entrades del "
"resultat ofereix:"

#: ../../geolocation_editor/geoeditor_search.rst:30
msgid ""
"**Copy Coordinates** which you can paste later to one or more images in the "
"image list under the map"
msgstr ""
"**Copia les coordenades**, les podeu enganxar més tard a una o més imatges "
"de la llista d'imatges a sota del mapa"

#: ../../geolocation_editor/geoeditor_search.rst:32
msgid ""
"**Move Selected Images To This Position** which is basically the same but "
"more straightforward provided you have the images already loaded into the "
"geolocation editor"
msgstr ""
"**Mou les imatges seleccionades a aquesta posició**, el qual és bàsicament "
"el mateix, però més senzill sempre que tingueu les imatges ja carregades a "
"l'Editor de geolocalització"

#: ../../geolocation_editor/geoeditor_search.rst:34
msgid ""
"**Remove From Results List** which is particular useful in conjunction with "
"the Keep-the-results-of-old-searches-... button above the results list."
msgstr ""
"**Elimina de la llista de resultats**, la qual és especialment útil en "
"conjunció amb el botó manté els resultats de les cerques antigues... que hi "
"ha a sobre de la llista de resultats."

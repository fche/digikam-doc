.. meta::
   :description: Export Items From Your Collections To External Media
   :keywords: digiKam, documentation, user manual, photo management, open source, free, help, learn, export, flickr

.. metadata-placeholder

   :authors: - digiKam Team

   :license: see Credits and License page for details (https://docs.digikam.org/en/credits_license.html)

.. _export_tools:

############
Export Tools
############
.. figure:: images/index_export_tools.webp
    :alt:

This section explain how to use the digiKam export tools.

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :glob:

   export_tools/flickr_export.rst

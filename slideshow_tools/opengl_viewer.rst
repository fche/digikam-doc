.. meta::
   :description: Using digiKam OpenGL Viewer
   :keywords: digiKam, documentation, user manual, photo management, open source, free, learn, easy, slide

.. metadata-placeholder

   :authors: - digiKam Team

   :license: see Credits and License page for details (https://docs.digikam.org/en/credits_license.html)

.. _opengl_viewer:

OpenGL Viewer
=============

.. contents::

This tool preview a series of items using OpenGL hardware to speed-up rendering. There is no configuration dialog. Calling this tool from View --> OpenGL Image Viewer will show items in fullscreen mode.

Usage from Keyboard and mouse to quickly navigate between items is listen below:

- Item Access

    Previous Item:
        Up key
        PgUp key
        Left key
        Mouse wheel up

    Next Item:
        Down key
        PgDown key
        Right key
        Mouse wheel down

    Quit:
        Esc key

- Item Display

    Toggle fullscreen to normal:
        f key

    Toggle scroll-wheel action:
        c key (either zoom or change image)

    Rotation:
        r key

    Reset view:
        double click

    Original size:
        o key

    Zooming:
        Move mouse in up-down-direction while pressing the right mouse button
        c key and use the scroll-wheel
        + and - keys
        ctrl + scrollwheel

    Panning:
        Move mouse while pressing the left button

- Others

    Show help dialog:
        F1 key
